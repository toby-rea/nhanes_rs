use futures::future::join_all;

mod scraping;
use crate::scraping::scrape_datasets;

#[cfg(not(target_arch = "wasm32"))]
#[tokio::main]
async fn main() {
    let components: [&str; 5] = [
        "Demographics",
        "Dietary",
        "Examination",
        "Laboratory",
        "Questionnaire",
    ];

    // Create the datasets folder if it doesn't exist
    std::fs::create_dir_all("datasets").unwrap();

    // Fetch datasets for each component
    let mut v = Vec::new();
    for component in components {
        v.push(scrape_datasets(component.to_string()));
    }
    join_all(v).await;
}
