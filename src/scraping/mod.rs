use scraper::{Html, Selector};

mod dataset;
use dataset::Dataset;

/// Scrapes the datasets for a given component.
///
/// # Arguments
///
/// * `component` - The component for which the datasets should be scraped.
///
pub async fn scrape_datasets(component: String) -> Result<(), reqwest::Error> {
    let response = reqwest::get(format!(
        "https://wwwn.cdc.gov/nchs/nhanes/search/datapage.aspx?Component={component}"
    ))
    .await?;

    let document = Html::parse_document(&response.text().await?);

    // Get the table rows
    let row_selector = Selector::parse("tbody:nth-child(2) > tr ").unwrap();
    let rows = document.select(&row_selector);

    // Iterate over the rows and extract the data
    let datasets: Vec<Dataset> = rows.map(extract_dataset_from_row).collect();
    write_datasets_to_file(datasets, component);

    Ok(())
}

/// Extracts the data from the table row.
///
/// # Arguments
///
/// * `row` - A table row from the NHANES website.
///
fn extract_dataset_from_row(row: scraper::element_ref::ElementRef) -> Dataset {
    let dates = row
        .select(&Selector::parse("td:nth-child(1)").unwrap())
        .next()
        .map(|x| x.text().collect::<String>().trim().to_owned());
    let title = row
        .select(&Selector::parse("td:nth-child(2)").unwrap())
        .next()
        .map(|x| x.text().collect::<String>().trim().to_owned());
    let docs_link = row
        .select(&Selector::parse("td:nth-child(3) > a").unwrap())
        .next()
        .and_then(|a| a.value().attr("href"))
        .map(str::to_owned);
    let data_link = row
        .select(&Selector::parse("td:nth-child(4) > a").unwrap())
        .next()
        .and_then(|a| a.value().attr("href"))
        .map(str::to_owned);

    // Split the date into start and end year
    let dates: Vec<&str> = dates.as_ref().unwrap().split('-').collect();
    let start_year = dates.first().map(|x| x.parse::<u16>().unwrap());
    let end_year = dates.get(1).map(|x| x.parse::<u16>().unwrap());

    // Create the dataset and prefix the links
    let mut dataset = Dataset {
        start_year,
        end_year,
        title,
        docs_link,
        data_link,
    };
    prefix_links(&mut dataset);

    dataset
}


/// Writes the datasets to a JSON file.
///
/// # Arguments
///
/// * `datasets` - A list of datasets to be written to a file.
/// * `component` - The component for which the datasets were fetched.
///
fn write_datasets_to_file(datasets: Vec<Dataset>, component: String) {
    let datasets_json = serde_json::to_string_pretty(&datasets).unwrap();
    std::fs::write(
        format!("datasets/{}.json", component.to_lowercase()),
        datasets_json,
    )
    .unwrap();
}

/// Alters a dataset to prefix the links with the CDC domain.
///
/// # Arguments
///
/// * `dataset` - A dataset whose links should be prefixed.
fn prefix_links(dataset: &mut Dataset) {
    if let Some(docs_link) = &dataset.docs_link {
        if docs_link.ends_with(".htm") {
            dataset.docs_link = Some(format!("https://wwwn.cdc.gov{docs_link}"));
        }
    }
    if let Some(data_link) = &dataset.data_link {
        let lowercase_link = data_link.to_lowercase();
        if lowercase_link.ends_with(".xpt") || lowercase_link.ends_with(".zip") {
            dataset.data_link = Some(format!("https://wwwn.cdc.gov{data_link}"));
        }
    }
}
