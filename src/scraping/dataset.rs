use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Dataset {
    pub start_year: Option<u16>,
    pub end_year: Option<u16>,
    pub title: Option<String>,
    pub docs_link: Option<String>,
    pub data_link: Option<String>,
}
